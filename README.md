# Running the notebook by itself

This is a simple example of running jupyter notebook (integrated with ROOT@CERN)

## Start the notebook

The following comand will:

- pull the correct image
- expose port 8888
- automathically remove the container when closed or stopped.

```bash
docker run -p8888:8888 atlasopendata/root_notebook:latest
```

Now we should have the notebook running on port 8888 on our docker host.
In this specific case, access via the browser by pasting in a browser the url provided by the terminal. It should be something like
(The token will be different for you)

```bash
 To access the notebook, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/nbserver-15-open.html
    Or copy and paste one of these URLs:
        http://4c61742ed77c:8888/?token=34b7f124f6783e047e796fea8061c3fca708a062a902c2f9
     or http://127.0.0.1:8888/?token=34b7f124f6783e047e796fea8061c3fca708a062a902c2f9
```



# Running JupyterHub in docker

This is a simple example of running jupyterhub in a docker container.

This example will:

- create a docker network
- run jupyterhub in a container
- run users in their own containers

It does not:

- enable persistent storage for users or the hub
- run the proxy in its own container

On a side note, this deployment uses Native Authenticator to authenticate users.
Native Authenticator provides the following features:


- New users can signup on the system;


- New users can be blocked of accessing the system and need an admin authorization;


- Option of increase password security by avoiding common passwords or minimum password length;


- Option to block users after a number attempts of login;


- Option of open signup and no need for initial authorization;


- Option of adding more information about users on signup.


By default, the user "admin" has administrator privileges.

## Initial setup

The first thing we are going to do is create a network for jupyterhub to use.

```bash
docker network create jupyterhub
```

Second, we are going to build our hub image. First, in a terminal cd into the `Jupyterhub` folder, then execute:

```bash
docker build -t hub .
```

We also want to pull the image that will be used:

```bash
docker pull atlasopendata/root_notebook
```

## Start the hub

To start the hub, we want to:

- run it on the docker network
- expose port 8000
- mount the host docker socket

```bash
docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock --net jupyterhub --name jupyterhub -p8000:8000 hub
```

Now we should have jupyterhub running on port 8000 [http://localhost:8000/](http://localhost:8000/) on our docker host.

## First time login
Once you access your URL, you should be redirected to a page similar to this:


<div align="center">
  <img src="./images/jh_login.png" />
</div>

You never accessed the environment before, so you need to register  first. Click on the "Signup!" link below the authenticator, and you'll be redirected to this page:

<div align="center">
  <img src="./images/jh_signup.png" />
</div>

**IMPORTANT** If you are responsible for the infrastructure, you should register using the username "admin", and choose your own password; this will grant you the rights to authorize other users registering to the environment. You can access the user management page by going at this address: [http://localhost:8000/hub/authorize](http://localhost:8000/hub/authorize). </br>
Once the registration is over, you should see this message:

<div align="center">
  <img src="./images/jh_signup_complete.png" />
</div>

Then just click on "Login!" to go back to the login page; wait for the admin to authorize your access and you're good to go!

---

## References
* https://jupyterhub-dockerspawner.readthedocs.io/en/latest/
* Docker Hub for development: https://hub.docker.com/r/atlasopendata/root_notebook
